import helper

alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"


def encrypt(plain_text: str, key: int):
    cipher_text = ""
    for i in range(len(plain_text)):
        plain_char = plain_text[i]
        cipher_char = alphabet[(alphabet.find(plain_char) + key) % len(alphabet)]
        cipher_text += cipher_char
    return cipher_text


def decrypt(cipher_text: str, key: int):
    return encrypt(cipher_text, -key)


functions = {
    "encrypt": [encrypt, (helper.get_text, "plain"), (helper.get_key, None)],
    "decrypt": [decrypt, (helper.get_text, "cipher"), (helper.get_key, None)]
}
