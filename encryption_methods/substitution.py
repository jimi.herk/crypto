import helper

alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"


def encrypt(plain_text: str, keyword: str):
    key = helper.generate_key(keyword)
    cipher_text = ""
    for i in range(len(plain_text)):
        cipher_text += key[alphabet.find(plain_text[i])]
    return cipher_text


def decrypt(cipher_text: str, keyword: str):
    key = helper.generate_key(keyword)
    plain_text = ""
    for i in range(len(cipher_text)):
        plain_text += alphabet[key.find(cipher_text[i])]
    return plain_text


functions = {
    "encrypt": [encrypt, (helper.get_text, "plain"), (helper.get_text, "key")],
    "decrypt": [decrypt, (helper.get_text, "cipher"), (helper.get_text, "key")]
}
