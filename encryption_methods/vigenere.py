import caesar
import helper

alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"


def encrypt(plain_text: str, keyword: str):
    cipher_text = ""
    for i in range(len(plain_text)):
        key = alphabet.find(keyword[(i % len(keyword))])
        cipher_text += caesar.encrypt(plain_text[i], key)
    return cipher_text


def decrypt(cipher_text: str, keyword: str):
    plain_text = ""
    for i in range(len(cipher_text)):
        key = alphabet.find(keyword[(i % len(keyword))])
        plain_text += caesar.decrypt(cipher_text[i], key)
    return plain_text


functions = {
    "encrypt": [encrypt, (helper.get_text, "plain"), (helper.get_text, "key")],
    "decrypt": [decrypt, (helper.get_text, "cipher"), (helper.get_text, "key")]
}
