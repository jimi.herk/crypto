import colorama
from colorama import Fore

alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
colorama.init()


def get_text(switch: str):
    checked = ""
    unchecked = input(f"{switch} text:").upper()
    for char in unchecked:
        if alphabet.find(char) != -1:
            checked += char
        elif char == " ":
            pass
        else:
            print(Fore.RED + "WARNING:" + Fore.RESET + " Some character were cut out because they can't be encrypted.")
    return checked


def get_key(placeholder):
    while True:
        unchecked = input("key:")
        if unchecked.isnumeric():
            return int(unchecked)
        else:
            print("the key has to be a number!")


def generate_key(keyword: str):
    key = ""
    for i in range(len(keyword)):
        if key.find(keyword[i]) != -1:
            pass
        else:
            key += keyword[i]
    for i in range(25, -1, -1):
        char = alphabet[i]
        if key.find(char) != -1:
            pass
        else:
            key += char
    return key
